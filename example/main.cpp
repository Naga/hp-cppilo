//#define	HPCPPILO_AS_DLL //uncomment to use hp_cppilo as dll (hp_cppilo has to be compiled as dll)
#include "../hp-cppilo.h"
#include <iostream>
#include <string>


int main() {
	std::string ip = "10.10.10.10";
	unsigned int port = 443;
	std::string user = "xxxxxxx";
	std::string pass = "xxxxxxx";

	hp_cppilo *ilo = new hp_cppilo(ip, port);
	std::cout << "Connecting to " << ip.c_str() << ":" << (int)port << " - ";

	if (!ilo->connect()) {
		std::cout << "Failed" << std::endl;
		exit(1);
	} else {
		std::cout << "Connected with SSL-" << ilo->getSSLCipher().c_str() << std::endl;
	}

	ilo->setLoginData(user, pass);

	std::cout << "Initializing - ";
	ilo->initialize();
	std::cout << "done." << std::endl;
	health_data *health = ilo->getHealthData();
	std::cout << "Power redundancy: " << ilo->getPowerRedundancy() << std::endl;


	std::cin.get();
	return 1;
}
