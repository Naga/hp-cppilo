#include <iostream>
#include <vector>

#pragma warning( disable: 4251 )

#ifndef _HPCPPILO_H
#define _HPCPPILO_H

class NSocket; //forward

#ifdef IN_HPCPPILO
	#ifdef BUILD_SHARED
		#define DLLAPI __declspec(dllexport)
	#endif
#else
	#ifdef HPCPPILO_AS_DLL
		#define DLLAPI __declspec(dllimport)
	#endif
#endif

#ifndef DLLAPI
	#define DLLAPI
#endif

struct power_supply {
	std::string label;
	std::string status;
};

struct fan {
	std::string label;
	std::string zone;
	std::string speed;
	std::string status;
	std::string speed_unit;
};

struct temp {
	std::string label;
	std::string location;
	std::string status;
	std::string currentreading;
	std::string caution;
	std::string critical;
	std::string unit;
};

struct health_at_a_glance {
	std::string fans_status;
	std::string fans_redundancy;
	std::string temp_status;
	std::string power_supplies_status;
	std::string power_supplies_redundancy;
};


struct health_data {
	std::vector<temp*>			temps;
	std::vector<fan*>			fans;
	std::vector<power_supply*>	power_supplies;
	health_at_a_glance			*healthataglance;
	health_data() : healthataglance(NULL) {}
};

#define DEFAULT_TIMEOUT 30 //seconds


struct network_data {
	bool		dhcp_dns_server;
	bool		dhcp_gateway;
	std::string dns_name;
	bool		dhcp_domain_name;
	std::string ip_address;
	std::string domain_name;
	unsigned	nic_speed;
	std::string mac_address;
	bool		full_duplex;
	bool		dhcp_enable;
	std::string primary_dns;
	std::string second_dns;
	std::string third_dns;
	std::string subnet_mask;
	std::string gateway_ip_address;
};

struct global_data {
	unsigned int	session_timeout;
	unsigned int	https_port;
	unsigned int	http_port;
	unsigned int	ssh_port;
	bool			ssh_status;
	bool			telnet_enabled;
	unsigned int	virtual_media_port;
	unsigned int	min_passwordlen;
};

struct fw_data {
	std::string version;
	std::string date;
	std::string processor;
};

struct ilo_return_struct{
	std::string status;
	std::string message;
	ilo_return_struct(std::string _status, std::string msg) :
		status(_status), message(msg) {}
};
typedef ilo_return_struct ilo_return;

#ifdef BUILD_SHARED
extern "C" {
#endif

class DLLAPI hp_cppilo {
private:
	std::string _username;
	std::string _password;
	std::string _ip;
	unsigned int _port;
	NSocket *socket;
	std::string last_error;

	health_data		*health_data_cache;
	network_data	*network_data_cache;
	fw_data			*fwdata;
	global_data		*globaldata;

	unsigned short int ilo_version;

public:
	hp_cppilo(std::string ip, unsigned int port);
	~hp_cppilo();

	bool connect();
	void disconnect();
	void initialize();
	bool initializeAll();

	bool refreshNetworkData();
	bool refreshHealthData();
	bool refreshFirmwareData();
	bool refreshGlobalData();

	void setLoginData(std::string username, std::string password) { _username = username; _password = password; }
	void setiLOVersion(unsigned short int version) { ilo_version = version; } //force set of version

	bool isConnected();

	std::string getLastError() {return last_error;}

	std::string getPowerRedundancy();

	std::vector<power_supply*> getPowerSupplies();
	std::vector<fan*> getFans();
	std::vector<temp*> getTemperatures();

	network_data*getNetworkData();
	fw_data		*getFwData();
	global_data *getGlobalData();
	health_data *getHealthData();
	unsigned int getPowerConsumption();

	unsigned short int getiLOVersion() {return ilo_version;}
	std::string getSSLCipher();
	
private:
	void _setiLOversion();
	void wrapCmd(std::string &cmd);
	void cleanupiLOXML(std::string &response);
	std::vector<ilo_return *> parse_response(std::string xml_response);
	std::string SendRequest(std::string request, unsigned int timeout_in_sec = DEFAULT_TIMEOUT);
	bool checkError(std::string response);

};

#ifdef BUILD_SHARED
}
#endif
#endif