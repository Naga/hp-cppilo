#include "NSocket.h"

#if defined(HAS_OPENSSL) && defined(WINDOWS)
	#include <openssl/applink.c> // To prevent crashing (see the OpenSSL FAQ)
#endif

NSocket::NSocket(std::string hostname, unsigned short int port, bool isSSL/* = false*/) {
	this->hostname = hostname;
	this->port = port;
	this->isSSL = isSSL;
	this->is_connected = false;
}

bool NSocket::initNsocket() {
#ifdef WIN32
	memset(&hWSAData, 0, sizeof(hWSAData));
	WSAStartup(MAKEWORD(2, 0), &hWSAData);
#endif
	if((_s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return false;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
#ifdef WINDOWS
	if(inet_pton(AF_INET, hostname_to_ip(hostname).c_str(), &(serv_addr.sin_addr))<=0)
#else
	if(inet_pton(AF_INET, hostname_to_ip(hostname).c_str(), &serv_addr.sin_addr)<=0)
#endif
		return true;
	else
		return false;
}

bool NSocket::start() {
#ifdef HAS_OPENSSL
	if (!isSSL) {
#endif
		if (Connect() == SOCKET_ERROR)
			return false;
		else
			return true;
#ifdef HAS_OPENSSL
	} else
		return ConnectSSL();
#endif
}

int NSocket::Connect() {
	initNsocket();
	if(::connect(_s, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		return SOCKET_ERROR;
	} else
		is_connected = true;
	return 1;
}

std::string NSocket::hostname_to_ip(std::string hostname) {
	struct hostent *he;
	struct in_addr **addr_list;
	int i;

	if ( (he = gethostbyname( hostname.c_str() ) ) == NULL)
		return "";

	addr_list = (struct in_addr **) he->h_addr_list;

	for(i = 0; addr_list[i] != NULL; i++) {
		return inet_ntoa(*addr_list[i]);
	}
	return "";
}

bool SetSocketBlockingEnabled(int fd, bool blocking) {
   if (fd < 0)
	return false;
#ifdef WIN32
	unsigned long mode = blocking ? 0 : 1;
	return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
#else
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) return false;
	flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
	return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
#endif
}

int NSocket::RecvUntil(std::string& data, std::string terminator) {
	if (!is_connected) 
		return SOCKET_ERROR;
	int len = 0;
	while (1) {
		char r;
		int retcode = recv(_s, &r, 1, 0);

		if (retcode <= 0) {
			Disconnect();
			break;
		}

		data += r;
		len++;

		if (data.find(terminator) != std::string::npos && data.find(terminator) == (data.length() - terminator.length()))
			break;
	}
	return len;
}

void NSocket::waitfordata(unsigned int timeout) {
	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(_s, &readfds);
	timeval timeouts;
	timeouts.tv_sec = timeout;
	timeouts.tv_usec = 0;

	select(_s+1, &readfds, NULL, NULL, &timeouts);
}

int NSocket::recv (SOCKET s, char *buf, int len, int flags) {
#ifdef HAS_OPENSSL
	if (isSSL) {
		return SSL_read(ssl, buf, len);
	}
#endif
	return ::recv(s, buf, len, flags);
}

int NSocket::Recv (std::string &str, int len) {
	if (!is_connected) 
		return SOCKET_ERROR;

	int glen = 0;
	while (glen <= len) {
		char r;
		int retcode = recv(_s, &r, 1, 0);

		if (retcode <= 0) {
			Disconnect();
			break;
		}

		str += r;
		len++;
	}
	return glen;
}

int NSocket::send (SOCKET s, const char *buf, int len, int flags) {
#ifdef HAS_OPENSSL
	if (isSSL) {
		return SSL_write(ssl, buf, len);
	}
#endif
	return ::send(s, buf, len, flags);
}

int NSocket::available() {
		unsigned long data_avail = 0;

        #ifdef WINDOWS
                if (ioctlsocket(_s, FIONREAD, &data_avail) != 0)
        #else 
                if (ioctl(_s, FIONREAD, &data_avail) != 0)
        #endif
					return SOCKET_ERROR;
	return data_avail;
}

int NSocket::SendLine(std::string data) {
	if ((data.length() >= strlen("\n")) && (data.substr(data.length()-strlen("\n")) != "\n"))
		data += "\n";

	return Send(data);
}

int NSocket::Send(std::string data) {
	if (!is_connected)
		return SOCKET_ERROR;
	int sent = send(_s, data.c_str(), data.length(), 0);
	return sent;
}

void NSocket::Close() {
#ifdef WINDOWS
	closesocket(_s);
#else
	close(_s);
#endif
}

void NSocket::Disconnect() {
#ifdef HAS_OPENSSL
	if (isSSL) 
		freeSSL();
#endif
	Close();
	is_connected = false;
}



//-------------- SSL STUFF
#ifdef HAS_OPENSSL

void NSocket::freeSSL() {
	SSL_free(ssl);
	SSL_CTX_free(ctx);
}

SSL_CTX*  NSocket::initSSLNsocket() {
	SSL_CTX *ctx;
	CRYPTO_malloc_init();
	SSL_library_init();
	SSL_load_error_strings();
	ERR_load_BIO_strings();
	OpenSSL_add_all_algorithms();

	
	ctx = SSL_CTX_new(SSLv3_client_method());

    if ( ctx == NULL ) {
        ERR_print_errors_fp(stderr);
        abort();
    }
	return ctx;
}

std::string NSocket::getSSLCipher() {
	return SSL_get_cipher(ssl);
}

bool NSocket::ConnectSSL() {
	if (Connect() == SOCKET_ERROR)
		return false;

	ctx = initSSLNsocket();
	ssl = SSL_new(ctx);
	SSL_set_fd(ssl, _s);
    if ( SSL_connect(ssl) == -1 /* FAIL */ ) {
        ERR_print_errors_fp(stderr);
		return false;
	} else {
		return true;
	}

}
#endif
