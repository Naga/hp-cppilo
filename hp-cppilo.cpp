#include <iostream>
#include <map>
#include <sstream>

#include "defines.h"

#define IN_HPCPPILO

#ifdef BUILD_SHARED
	#define AS_DLL
#endif


#include "hp-cppilo.h"
#include "NSocket.h"
#include "PugiXML\pugixml.hpp"

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = str.find(from);
	while (start_pos != std::string::npos) {
		start_pos = str.find(from);
		if(start_pos == std::string::npos)
			return;
		str.replace(start_pos, from.length(), to);
		start_pos = str.find(from);
	}
}

template<typename T>
static void deletePtrVector(std::vector<T> ptrc) {
	while(!ptrc.empty()) {
		delete ptrc.back();
		ptrc.pop_back();
	}
}

hp_cppilo::hp_cppilo(std::string ip, unsigned int port) {
	_ip = ip;
	_port = port;
	ilo_version = (unsigned short)-1;
	health_data_cache = NULL;
	network_data_cache = NULL;
	fwdata = NULL;
	globaldata = NULL;
}

hp_cppilo::~hp_cppilo() {
	deletePtrVector(health_data_cache->fans);
	deletePtrVector(health_data_cache->temps);
	deletePtrVector(health_data_cache->power_supplies);
	delete health_data_cache->healthataglance;
	delete health_data_cache;
	delete network_data_cache;
	delete fwdata;
	delete globaldata;
}
bool hp_cppilo::isConnected() {
	return (socket != NULL) ? socket->Connected() : false;
}

std::string hp_cppilo::getSSLCipher() {
	return socket->getSSLCipher();
}

bool hp_cppilo::connect() {
	socket = new NSocket(_ip, _port, true);
	if (!socket->start())
		return false;
	return true;
}

void hp_cppilo::disconnect() {
	socket->Disconnect();
}


void hp_cppilo::_setiLOversion() {
	std::stringstream request;
	request << "POST /ribcl HTTP/1.1\r\n";
	request << "HOST: "<< _ip << ":" << _port << " \r\n";
	request << "Content-length: 30\r\n";
	request << "Connection: Close\r\n";
	request << "<RIBCL VERSION=\"2.0\"></RIBCL>\r\n\r\n";

	std::string response = SendRequest(request.str());

	if (response.find("HTTP/1.1 200 OK") != std::string::npos) {
        ilo_version = 3;
    } else {
        ilo_version = 2;
    }
}


void hp_cppilo::wrapCmd(std::string &cmd) {
	std::stringstream request;
	std::string xmlrequest;

	xmlrequest =	"<RIBCL VERSION=\"2.21\">"
					"<LOGIN USER_LOGIN=\""+_username+"\" PASSWORD=\""+_password+"\">";
	xmlrequest +=				cmd;
	xmlrequest +=				"</LOGIN>"
							"</RIBCL>"
							"\r\n\r\n";

	int len = xmlrequest.size();

	if (ilo_version == 2) {
		request  << "<?xml version=\"1.0\"?>";
	} else {
		request << "POST /ribcl HTTP/1.1\r\n";
		request << "HOST: "<< _ip << ":" << _port << " \r\n";
		request << "Content-length: "<< len << "\r\n";
		request << "Connection: Close\r\n";
	}
	request << xmlrequest.c_str();
	cmd = request.str();
}

void hp_cppilo::cleanupiLOXML(std::string &response) {
	replaceAll(response,"<RIBCL VERSION=\"2.22\"/>", "<RIBCL VERSION=\"2.22\">");
	replaceAll(response,"<?xml version=\"1.0\"?>", "");
}

bool hp_cppilo::checkError(std::string response) {
	if (response.empty()) {
		return true;
	}
	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());
	pugi::xpath_node node = doc.select_single_node("//RIBCL/RESPONSE[@STATUS ='0x0001']");

	if (node) {
		last_error = node.node().attribute("MESSAGE").as_string();
		return true;
	}
	last_error = "";
	return false;	
}

std::string hp_cppilo::SendRequest(std::string request, unsigned int timeout_in_sec) {
	if (!isConnected())
		if (!connect()) 
			return "ERROR";

	socket->Send(request);
	socket->waitfordata(timeout_in_sec);

	std::string ret;
	socket->Recv(ret, socket->available());
	cleanupiLOXML(ret);
	return ret;
}

bool hp_cppilo::refreshNetworkData() {
	if (!network_data_cache)
		network_data_cache = new network_data();

	std::string request =	"<RIB_INFO MODE=\"read\">"
							"<GET_NETWORK_SETTINGS/>"
							"</RIB_INFO>";
	wrapCmd(request);
	std::string response = SendRequest(request);

	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());

	if (checkError(response)) {
		return false;
	}

	pugi::xpath_node xnode = doc.select_single_node("//GET_NETWORK_SETTINGS");
	pugi::xml_node node = xnode.node();


	network_data_cache->dhcp_dns_server =		(strcmp(node.child("DHCP_DNS_SERVER").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	network_data_cache->dhcp_domain_name =		(strcmp(node.child("DHCP_DOMAIN_NAME").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	network_data_cache->dhcp_enable =			(strcmp(node.child("DHCP_ENABLE").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	network_data_cache->dhcp_gateway =			(strcmp(node.child("DHCP_GATEWAY").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	network_data_cache->dns_name =				node.child("DNS_NAME").attribute("VALUE").as_string();
	network_data_cache->domain_name	=			node.child("DOMAIN_NAME").attribute("VALUE").as_string();
	network_data_cache->full_duplex =			(strcmp(node.child("FULL_DUPLEX").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	network_data_cache->gateway_ip_address =	node.child("GATEWAY_IP_ADDRESS").attribute("VALUE").as_string();
	network_data_cache->ip_address =			node.child("IP_ADDRESS").attribute("VALUE").as_string();
	network_data_cache->mac_address =			node.child("MAC_ADDRESS").attribute("VALUE").as_string();
	network_data_cache->nic_speed =				node.child("NIC_SPEED").attribute("VALUE").as_uint();
	network_data_cache->primary_dns =			node.child("PRIM_DNS_SERVER").attribute("VALUE").as_string();
	network_data_cache->second_dns =			node.child("SEC_DNS_SERVER").attribute("VALUE").as_string();
	network_data_cache->third_dns =				node.child("TER_DNS_SERVER").attribute("VALUE").as_string();
	network_data_cache->subnet_mask =			node.child("SUBNET_MASK").attribute("VALUE").as_string();
	return true;
}

bool hp_cppilo::refreshFirmwareData() {
	if (!fwdata)
		fwdata = new fw_data();

	std::string request =	"<RIB_INFO MODE=\"read\">"
							"<GET_FW_VERSION/>"
							"</RIB_INFO>";
	wrapCmd(request);
	std::string response = SendRequest(request);

	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());

	if (checkError(response)) {
		return false;
	}

	pugi::xml_node fwdata_node = doc.select_single_node("//GET_FW_VERSION").node();

	fwdata->date = fwdata_node.attribute("FIRMWARE_DATE").as_string();
	fwdata->version = fwdata_node.attribute("FIRMWARE_VERSION").as_string();
	fwdata->processor = fwdata_node.attribute("MANAGEMENT_PROCESSOR").as_string();
	return true;
}

bool hp_cppilo::refreshHealthData() {
	if(!health_data_cache)
		health_data_cache = new health_data();

	health_data_cache->fans.clear();
	health_data_cache->temps.clear();
	health_data_cache->power_supplies.clear();
	if (health_data_cache->healthataglance)
		delete health_data_cache->healthataglance;


	std::string request =	"<SERVER_INFO MODE=\"read\">"
							"<GET_EMBEDDED_HEALTH/>"
							"</SERVER_INFO>";

	wrapCmd(request);
	std::string response = SendRequest(request);

	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());

	if (checkError(response)) {
		delete health_data_cache;
		return false;
	}


	pugi::xpath_node_set fan_nodes = doc.select_nodes("//GET_EMBEDDED_HEALTH_DATA/FANS/FAN");
	pugi::xpath_node_set temp_nodes = doc.select_nodes("//GET_EMBEDDED_HEALTH_DATA/TEMPERATURE/TEMP");
	pugi::xpath_node_set power_nodes = doc.select_nodes("//GET_EMBEDDED_HEALTH_DATA/POWER_SUPPLIES/SUPPLY");
	

	for (pugi::xpath_node_set::const_iterator it = fan_nodes.begin(); it != fan_nodes.end(); ++it) {
		pugi::xml_node node = (*it).node();
		fan *f = new fan();
		f->label = node.child("LABEL").attribute("VALUE").as_string();
		f->status = node.child("STATUS").attribute("VALUE").as_string();
		f->zone = node.child("ZONE").attribute("VALUE").as_string();
		f->speed = node.child("SPEED").attribute("VALUE").as_string();
		f->speed_unit = node.child("SPEED").attribute("UNIT").as_string();
		health_data_cache->fans.push_back(f);
	}
	for (pugi::xpath_node_set::const_iterator it = temp_nodes.begin(); it != temp_nodes.end(); ++it) {
		pugi::xml_node node = (*it).node();
		temp *t = new temp();
		t->label = node.child("LABEL").attribute("VALUE").as_string();
		t->location = node.child("LOCATION").attribute("VALUE").as_string();
		t->status = node.child("STATUS").attribute("VALUE").as_string();
		t->currentreading = node.child("CURRENTREADING").attribute("VALUE").as_string();
		t->caution = node.child("CAUTION").attribute("VALUE").as_string();
		t->critical = node.child("CRITICAL").attribute("VALUE").as_string();
		t->unit = node.child("CURRENTREADING").attribute("UNIT").as_string();
		health_data_cache->temps.push_back(t);
	}
	for (pugi::xpath_node_set::const_iterator it = power_nodes.begin(); it != power_nodes.end(); ++it) {
		pugi::xml_node node = (*it).node();
		power_supply *p = new power_supply();
		p->label = node.child("LABEL").attribute("VALUE").as_string();
		p->status = node.child("STATUS").attribute("VALUE").as_string();
		health_data_cache->power_supplies.push_back(p);
	}

	health_data_cache->healthataglance = new health_at_a_glance();
	health_data_cache->healthataglance->fans_redundancy = doc.select_single_node("//GET_EMBEDDED_HEALTH_DATA/HEALTH_AT_A_GLANCE/FANS[@REDUNDANCY]").node().attribute("REDUNDANCY").as_string();
	health_data_cache->healthataglance->fans_status = doc.select_single_node("//GET_EMBEDDED_HEALTH_DATA/HEALTH_AT_A_GLANCE/FANS[@STATUS]").node().attribute("STATUS").as_string();
	health_data_cache->healthataglance->power_supplies_redundancy = doc.select_single_node("//GET_EMBEDDED_HEALTH_DATA/HEALTH_AT_A_GLANCE/POWER_SUPPLIES[@REDUNDANCY]").node().attribute("REDUNDANCY").as_string();
	health_data_cache->healthataglance->power_supplies_status = doc.select_single_node("//GET_EMBEDDED_HEALTH_DATA/HEALTH_AT_A_GLANCE/POWER_SUPPLIES[@STATUS]").node().attribute("STATUS").as_string();
	health_data_cache->healthataglance->temp_status = doc.select_single_node("//GET_EMBEDDED_HEALTH_DATA/HEALTH_AT_A_GLANCE/TEMPERATURE").node().attribute("STATUS").as_string();

	return true;
}

bool hp_cppilo::refreshGlobalData() {
	if (!globaldata)
		globaldata = new global_data();

	std::string request =	"<RIB_INFO MODE=\"read\">"
							"<GET_GLOBAL_SETTINGS/>"
							"</RIB_INFO>";
	wrapCmd(request);
	std::string response = SendRequest(request);

	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());

	if (checkError(response)) {
		return false;
	}

	pugi::xml_node gdata_node = doc.select_single_node("//GET_GLOBAL_SETTINGS").node();

	globaldata->https_port = gdata_node.child("HTTPS_PORT").attribute("VALUE").as_uint();
	globaldata->http_port = gdata_node.child("HTTP_PORT").attribute("VALUE").as_uint();
	globaldata->min_passwordlen = gdata_node.child("MIN_PASSWORD").attribute("VALUE").as_uint();
	globaldata->session_timeout = gdata_node.child("SESSION_TIMEOUT").attribute("VALUE").as_uint();
	globaldata->ssh_port = gdata_node.child("SSH_PORT").attribute("VALUE").as_uint();
	globaldata->ssh_status = (strcmp(gdata_node.child("SSH_STATUS").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	globaldata->telnet_enabled = (strcmp(gdata_node.child("TELNET_ENABLE").attribute("VALUE").as_string(), "Y") == 0) ? true : false;
	globaldata->virtual_media_port = gdata_node.child("VIRTUAL_MEDIA_PORT").attribute("VALUE").as_uint();
	return true;
}

void hp_cppilo::initialize() {
	_setiLOversion();
}

bool hp_cppilo::initializeAll() {
	if (ilo_version == -1)
		_setiLOversion();

	return (refreshNetworkData() && refreshHealthData() && refreshGlobalData() && refreshFirmwareData());
}

std::string hp_cppilo::getPowerRedundancy() {
	if (!health_data_cache)
		refreshHealthData();

	return health_data_cache->healthataglance->power_supplies_redundancy;
}

std::vector<power_supply*> hp_cppilo::getPowerSupplies() {
	if (!health_data_cache)
		refreshHealthData();

	return health_data_cache->power_supplies;
}

network_data *hp_cppilo::getNetworkData() {
	if (!network_data_cache) 
		refreshNetworkData();

	return network_data_cache;
}

fw_data *hp_cppilo::getFwData() {
	if (!fwdata) 
		refreshFirmwareData();

	return fwdata;
}

global_data *hp_cppilo::getGlobalData() {
	if (!globaldata) 
		refreshGlobalData();

	return globaldata;
}

health_data *hp_cppilo::getHealthData() {
	if (!health_data_cache) 
		refreshHealthData();

	return health_data_cache;
}

unsigned int hp_cppilo::getPowerConsumption() {
	std::string request =	"<SERVER_INFO MODE=\"read\">"
							"<GET_POWER_READINGS/>"
							"</SERVER_INFO>";

	wrapCmd(request);

	std::string response = SendRequest(request);
	pugi::xml_document doc;
	doc.load_buffer(response.c_str(), response.size());

	if (checkError(response)) {
		delete health_data_cache;
		return false;
	}

	pugi::xml_node node = doc.select_single_node("//GET_POWER_READINGS/PRESENT_POWER_READING").node();

	return node.attribute("VALUE").as_uint();
}