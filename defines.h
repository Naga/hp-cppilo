#if defined(_WIN32) || defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__)
        #define WINDOWS
        #define OS_TYPE "Windows"
#elif defined(__linux__) || defined(linux) || defined(__linux)
        #define LINUX
        #define OS_TYPE "Linux"
#else
        #error This Program is made for Windows & Linux only
#endif

#ifdef WINDOWS
        #define snprintf _snprintf
        #define strcasecmp _stricmp 
        #define usleep(a) Sleep(a)
#else
        #define MAXHOSTNAMELEN 256
        #define SOCKET int
        #define vsprintf_s vsprintf //HUCK!
#endif

#define LINE_ENDING "\n"

#define HAS_OPENSSL
